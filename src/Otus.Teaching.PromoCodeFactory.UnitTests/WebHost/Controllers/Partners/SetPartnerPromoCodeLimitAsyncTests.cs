﻿using AutoFixture;
using AutoFixture.AutoMoq;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Controllers;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
    public class SetPartnerPromoCodeLimitAsyncTests
    {
        private readonly Mock<IRepository<Partner>> _partnersRepositoryMock;
        private readonly PartnersController _partnersController;
        private readonly Fixture _fixture;
        public SetPartnerPromoCodeLimitAsyncTests()
        {
            var fixture = new Fixture().Customize(new AutoMoqCustomization());
            _fixture = new Fixture();
            _partnersRepositoryMock = fixture.Freeze<Mock<IRepository<Partner>>>();
            
            _partnersController = fixture.Build<PartnersController>().OmitAutoProperties().Create();
        }

        private Partner GetActivePartnerWithLimit()
        {
            Partner partner = _fixture.Build<Partner>().Without(p => p.PartnerLimits).Create();
            partner.IsActive = true;
            partner.PartnerLimits = new List<PartnerPromoCodeLimit> { new PartnerPromoCodeLimit()
                    {
                        Id = Guid.Parse("e00633a5-978a-420e-a7d6-3e1dab116393"),
                        CreateDate = new DateTime(2020, 07, 9),
                        EndDate = new DateTime(2020, 10, 9),
                        Limit = 100
                    }};

            return partner;
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_PartnerIsNotFound_ReturnsNotFound()
        {
            //Arrange
            var partnerId = Guid.Parse("85a0f8ed-e7c2-4e5e-bac8-7567fee96c8b");
            Partner partner = null;

            _partnersRepositoryMock.Setup(x => x.GetByIdAsync(partnerId)).ReturnsAsync(partner);
            //Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, null);
            //Assert
            result.Should().BeAssignableTo<NotFoundResult>();
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_PartnerIsNotActive_ReturnsBadRequest()
        {
            //Arrange
            var partnerId = Guid.Parse("639bd6b5-b571-4f78-a335-c2fad39e1678");
            Partner partner = _fixture.Build<Partner>().Without(p => p.PartnerLimits).Create();
            partner.IsActive = false;

            _partnersRepositoryMock.Setup(x => x.GetByIdAsync(partnerId)).ReturnsAsync(partner);
            //Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, null);
            //Assert
            result.Should().BeAssignableTo<BadRequestObjectResult>();
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_ApplyingLimit_ShouldNullifyIssuedPromocodes()
        {
            //Arrange
            var partnerId = Guid.Parse("0356f6b8-67ca-4beb-be57-ba7f57cccb4d");
            Partner partner = GetActivePartnerWithLimit();
            SetPartnerPromoCodeLimitRequest request = _fixture.Create<SetPartnerPromoCodeLimitRequest>();
            request.Limit = 50;

            _partnersRepositoryMock.Setup(x => x.GetByIdAsync(partnerId)).ReturnsAsync(partner);
            //Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, request);
            //Assert
            partner.NumberIssuedPromoCodes.Should().Be(0);
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_ApplyingLimit_ShouldCancelPreviousLimit()
        {
            //Arrange
            var partnerId = Guid.Parse("363c71f1-053c-4ebd-b2f4-df9c562bdf5b");
            Partner partner = GetActivePartnerWithLimit();
            SetPartnerPromoCodeLimitRequest request = _fixture.Create<SetPartnerPromoCodeLimitRequest>();

            _partnersRepositoryMock.Setup(x => x.GetByIdAsync(partnerId)).ReturnsAsync(partner);
            //Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, request);
            //Assert
            partner.PartnerLimits.First(x => x.Id == Guid.Parse("e00633a5-978a-420e-a7d6-3e1dab116393")).CancelDate.Should().NotBeNull();
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_LimitIsZero_ReturnsBadRequest()
        {
            //Arrange
            var partnerId = Guid.Parse("abd2deb9-af18-42b0-953f-23621faabaa0");
            Partner partner = GetActivePartnerWithLimit();

            SetPartnerPromoCodeLimitRequest request = _fixture.Create<SetPartnerPromoCodeLimitRequest>();
            request.Limit = 0;
            _partnersRepositoryMock.Setup(x => x.GetByIdAsync(partnerId)).ReturnsAsync(partner);
            //Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, request);
            //Assert
            result.Should().BeAssignableTo<BadRequestObjectResult>();
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_ApplyingLimit_ShouldSaveToDataBase()
        {
            //Arrange
            var partnerId = Guid.Parse("abd2deb9-af18-42b0-953f-23621faabaa0");
            Partner partner = GetActivePartnerWithLimit();
            Partner savedPartner = new Partner();
            SetPartnerPromoCodeLimitRequest request = _fixture.Create<SetPartnerPromoCodeLimitRequest>();

            _partnersRepositoryMock.Setup(x => x.GetByIdAsync(partnerId)).ReturnsAsync(partner);
            _partnersRepositoryMock.Setup(s => s.UpdateAsync(It.IsAny<Partner>())).Callback((Partner p) => savedPartner = p);
            //Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, request);
            //Assert
            savedPartner.PartnerLimits.Should().NotBeEmpty();
        }
    }
}